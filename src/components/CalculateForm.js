import styled from "styled-components";
const FormCalculate = styled.form`
    background-color: white;
    display:flex;
    justify-content: center;
    align-items: center;
    width: fit-content;
    box-shadow: 5px 5px 5px 5px rgb(74,134,130);
    border-radius: 5px;
    margin-top: 100px;
    padding: 10px;
`
export default FormCalculate;